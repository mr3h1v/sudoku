// 模型字符集包含英文数字与标点, 我们忽略所有非数字字符将其视为0
const ocr_character = [
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '0', '0', '0', '0', '0', '0', '0', '0',
    '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0',
    '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0',
    '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0',
    '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0',
];

let session, rawImage = new Image(), boxes = [];
const cells = []; // 保存表格单元格的二维数组
const imageElm = document.getElementById('inputImage');
const canvasElm = document.getElementById('canvasImage');
const statusElm = document.getElementById('boardStatus');
window.cells = cells;
window.boxes = boxes;

for (let i = 0; i < 9; i++) {
    const row = document.getElementById('sudokuBoard').insertRow();
    cells.push([]);
    for (let j = 0; j < 9; j++) {
        const cell = row.insertCell();
        cell.contentEditable = false;
        cells[i].push(cell);
    }
}

class recPostprocess {
    constructor(preds) {
        this.ocr_character = ocr_character;
        this.preds_idx = [];
        const pred_len = 97;
        for (let i = 0; i < preds.length; i += pred_len) {
            const tmpArr = preds.slice(i, i + pred_len);
            const tmpMax = Math.max(...tmpArr);
            const tmpIdx = tmpArr.indexOf(tmpMax);
            this.preds_idx.push(tmpIdx);
        }
    }

    decode(text_index, is_remove_duplicate = false) {
        const ignored_tokens = this.get_ignored_tokens();
        const char_list = [];
        for (let idx = 0; idx < text_index.length; idx++) {
            if (ignored_tokens.includes(text_index[idx])) {
                continue;
            }
            if (is_remove_duplicate) {
                if (idx > 0 && text_index[idx - 1] === text_index[idx]) {
                    continue;
                }
            }
            char_list.push(this.ocr_character[text_index[idx] - 1]);
        }
        return char_list;
    }

    get_ignored_tokens() { return [0]; }

    outputResult() {
        return this.decode(this.preds_idx, true);
    }
}

export const recognize = async function (img) {
    const tensor = await ort.Tensor.fromImage(img, { tensorFormat: "RGB", norm: { bias: -127.5, mean: 127.5 } });
    const feeds = {};
    feeds[session.inputNames[0]] = tensor;

    const output = (await session.run(feeds))[session.outputNames[0]];
    const results = new recPostprocess(output.data);
    tensor.dispose();
    output.dispose();

    return results.outputResult();
}

const dbName = 'modelStore';
const storeName = 'paddle';
const modelName = 'en_PPOCRv4';
const totalSize = 7671960;

async function readFromIndexedDB(dbName, storeName, key) {
    return new Promise((resolve, reject) => {
        // 打开数据库
        const openRequest = indexedDB.open(dbName);
        openRequest.onerror = (event) => {
            reject('无法打开数据库: ' + event.target.errorCode);
        };

        openRequest.onupgradeneeded = function (event) {
            const db = event.target.result;
            if (!db.objectStoreNames.contains(storeName)) {
                db.createObjectStore(storeName);
                console.log('对象存储空间已创建');
            }
        }

        openRequest.onsuccess = (event) => {
            const db = event.target.result;
            const transaction = db.transaction(storeName, 'readonly');
            const store = transaction.objectStore(storeName);
            const getRequest = store.get(key);

            getRequest.onerror = (event) => {
                reject('读取失败: ' + event.target.errorCode);
            };

            getRequest.onsuccess = (event) => {
                if ((event.target.result) && (event.target.result.size === totalSize)) {
                    console.log('成功从 IndexedDB 中读取 Blob 数据', event.target.result);
                    resolve(event.target.result);
                } else {
                    downloadFileInChunks(db, storeName, modelName).then((ret) => resolve(ret));
                }
            };
        };
    });
}

async function downloadFileInChunks(database, storeName, modelName) {
    const urls = ['/model.00.ts', '/model.01.ts', '/model.02.ts', '/model.03.ts', '/model.04.ts'];

    const fetchPromises = urls.map(url => fetch(url));

    return Promise.all(fetchPromises)
        .then(responses => {
            return Promise.all(responses.map(response => response.blob()));
        })
        .then(data => {
            const dbTransaction = database.transaction(storeName, 'readwrite');
            const objectStore = dbTransaction.objectStore(storeName);
            console.log(data);
            const blob = new Blob(data);
            objectStore.put(blob, modelName);
            console.log('模型文件下载完成');
            return blob
        })
        .catch(error => {
            // 如果任何一个请求失败，这里会捕捉到错误
            console.error("Error fetching data: ", error);
        });
}

async function init() {

    let modelBlob;
    await readFromIndexedDB(dbName, storeName, modelName).then((ret) => { modelBlob = ret; });
    ort.env.webgl.pack = true;
    ort.env.logLevel = 'verbose';
    ort.env.trace = true;
    ort.env.debug = true;
    const reader = new FileReader();
    reader.onload = function (event) {
        ort.InferenceSession.create(event.target.result)
            .then((ret) => {
                session = ret;
                document.getElementById('onnxStatus').innerHTML = 'PaddleOCR 加载完成';
            })
            .catch((err) => {
                document.getElementById('onnxStatus').innerHTML = 'PaddleOCR 加载失败,请刷新页面重试';
            });
    };
    reader.onerror = function(err) {
        document.getElementById('onnxStatus').innerHTML = 'PaddleOCR 加载失败,请刷新页面重试';
    }
    reader.readAsArrayBuffer(modelBlob);
}

function checkPaddleOCRLoaded() {
    if (window.ort) {
        init();
    } else {
        // 等待一段时间后再次检查
        setTimeout(checkPaddleOCRLoaded, 100);
    }
};

function resetWorkspace() {
    imageElm.src = '';
    rawImage.src = '';
    statusElm.innerHTML = '';
    // 清空整个画布
    const ctx = canvasElm.getContext('2d');
    ctx.clearRect(0, 0, canvasElm.width, canvasElm.height);
    boxes = [];

    for (let i = 0; i < 9; i++) {
        for (let j = 0; j < 9; j++) {
            cells[i][j].style.backgroundColor = null;
            cells[i][j].textContent = null;
            cells[i][j].contentEditable = true;
        }
    }
};

function isValidSudoku(cells) {
    const rows = new Array(9).fill(0).map(() => new Array(9).fill({}));
    const columns = new Array(9).fill(0).map(() => new Array(9).fill({}));
    const subboxes = new Array(3).fill(0).map(() => new Array(3).fill(0).map(() => new Array(9).fill({})));
    const duplicated = new Array();
    let valid = true;
    for (let i = 0; i < 9; i++) {
        for (let j = 0; j < 9; j++) {
            const cell = cells[i][j];
            const c = cell.textContent;
            cell.style.backgroundColor = (cell.contentEditable == 'true') ? null : 'rgb(200,200,200)';
            // 多字符 非数字字符
            if ((c.length > 1) || (c.charCodeAt() < 49) || (c.charCodeAt() > 57)) {
                valid = false;
                duplicated.push({ x: i, y: j });
                continue
            }
            if (c !== '') {
                const index = c.charCodeAt() - '0'.charCodeAt() - 1;
                const ri = rows[i][index];
                const ci = columns[j][index];
                const bi = subboxes[Math.floor(i / 3)][Math.floor(j / 3)][index];
                if (ri.x >= 0) {
                    duplicated.push(ri);
                }
                if (ci.x >= 0) {
                    duplicated.push(ci);
                }
                if (bi.x >= 0) {
                    duplicated.push(bi);
                }
                if ((ri.x >= 0) || (ci.x >= 0) || (bi.x >= 0)) {
                    duplicated.push({ x: i, y: j });
                    valid = false;
                }
                rows[i][index] = { x: i, y: j };
                columns[j][index] = { x: i, y: j };
                subboxes[Math.floor(i / 3)][Math.floor(j / 3)][index] = { x: i, y: j };
            } else {
                valid = false;
            }
        }
    }
    duplicated.forEach((e) => {
        cells[e.x][e.y].style.backgroundColor = 'red';
    });
    return valid;
};

function preprocess(mat) {
    const dstMat = new cv.Mat();
    cv.cvtColor(mat, dstMat, cv.COLOR_GRAY2BGRA, 4);
    const dsize = new cv.Size(Math.floor(mat.cols / mat.rows * 48), 48);
    cv.resize(dstMat, dstMat, dsize, 0, 0, cv.INTER_LINEAR);

    const imgData = new ImageData(new Uint8ClampedArray(dstMat.data), dstMat.cols, dstMat.rows);
    dstMat.delete();
    return imgData;
}

function cleanContours(contours) {
    for (let i = 0; i < contours.size(); ++i) {
        const mat = contours.get(i);
        mat.delete(); // 释放cv.Mat对象占用的内存
    }
    contours.delete();
}

function findLines(img) {
    const meanHeight = Math.ceil(img.rows / 9);
    const meanWidth = Math.ceil(img.cols / 9);
    // 去除网格线
    let kernel = cv.getStructuringElement(cv.MORPH_RECT, new cv.Size(Math.ceil(img.rows / 9), 1));
    const tmpImg = new cv.Mat();
    // 查找横线
    const hContours = new cv.MatVector();
    const hierarchy = new cv.Mat();
    cv.morphologyEx(img, tmpImg, cv.MORPH_OPEN, kernel);
    cv.findContours(tmpImg, hContours, hierarchy, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE);

    // 查找竖线
    const vContours = new cv.MatVector();
    tmpImg.setTo(new cv.Scalar(0, 0, 0));
    kernel = cv.getStructuringElement(cv.MORPH_RECT, new cv.Size(1, Math.ceil(img.rows / 9)));
    cv.morphologyEx(img, tmpImg, cv.MORPH_OPEN, kernel);
    cv.findContours(tmpImg, vContours, hierarchy, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE);
    hierarchy.delete();
    tmpImg.delete();

    const hLines = [];
    const vLines = [];
    for (let i = 0; i < hContours.size(); i++) {
        const rect = cv.boundingRect(hContours.get(i));
        if ((hContours.size() > 10) && (hLines.length > 0) && (Math.abs(hLines[hLines.length - 1].y - rect.y) < (meanHeight / 3))) {
            continue
        }
        hLines.push(rect);
    }
    for (let i = 0; i < vContours.size(); i++) {
        const rect = cv.boundingRect(vContours.get(i));
        if ((vLines.length > 0) && (vContours.size() > 10) && (Math.abs(vLines[vLines.length - 1].x - rect.x) < (meanWidth / 3))) {
            continue
        }
        vLines.push(rect);
    }

    vLines.reverse();
    hLines.reverse();

    const color = new cv.Scalar(0, 0, 0);
    cv.drawContours(img, vContours, -1, color, -1, cv.LINE_8);
    cv.drawContours(img, vContours, -1, color, 2, cv.LINE_8);
    cv.drawContours(img, hContours, -1, color, -1, cv.LINE_8);
    cv.drawContours(img, hContours, -1, color, 2, cv.LINE_8);

    cleanContours(hContours);
    cleanContours(vContours);

    return { h: hLines, v: vLines };
}

function findBoxes(lines, xOffset = 0, yOffset = 0) {
    const boxes = [];
    for (let i = 0; i < 9; i++) {
        for (let j = 0; j < 9; j++) {
            const left = lines.v[j], right = lines.v[j + 1], top = lines.h[i], btm = lines.h[i + 1];
            const x = left.x + left.width, y = top.y + top.height
            const w = right.x - x, h = btm.y - y;
            boxes.push({ x: x + xOffset, y: y + yOffset, width: w, height: h, isEmpty: false });
        }
    }
    return boxes;
}

async function recognizeByRow(img, lines) {
    const board = [];
    const meanHeight = Math.ceil(img.rows / 9);
    for (let i = 0; i < 9; i++) {
        const y = lines.h[i].y + lines.h[i].height;
        const row = img.roi({ x: lines.v[0].width, y: y, width: lines.v[9].x, height: Math.min(meanHeight, img.rows - y) });
        const rowNums = await recognize(preprocess(row));
        row.delete();
        if (rowNums.length !== 9) {
            return { full: false };
        }
        board.push(rowNums);
    }
    return { board: board, full: true };
}

async function recognizeByBoxes(img, boxes) {
    const board = [];
    for (let i = 0; i < 9; i++) {
        board.push(Array(9).fill('0'));
    }
    let full = true;
    for (let i = 0; i < boxes.length; ++i) {
        const x = Math.floor(i / 9);
        const y = i % 9;

        const box = img.roi(boxes[i]);
        if (cv.countNonZero(box) > 3) {
            const digits = await recognize(preprocess(box));
            board[x][y] = digits[0];
        } else {
            boxes[i].isEmpty = true;
            full = false;
        }
        box.delete();
    }
    return { board: board, full: full };
}

function fillIn(board, cells) {
    for (let i = 0; i < board.length; i++) {
        const row = board[i];
        for (let j = 0; j < row.length; j++) {
            if (board[i][j] !== '0') {
                cells[i][j].textContent = board[i][j];
                cells[i][j].contentEditable = false;
                cells[i][j].style.backgroundColor = 'rgb(200,200,200)';
            }
        }
    }
}

window.genOutputPic = function () {
    const ctx = canvasElm.getContext('2d');

    canvasElm.width = rawImage.width;
    canvasElm.height = rawImage.height;
    // 在canvas上绘制内容
    ctx.drawImage(rawImage, 0, 0);
    ctx.fillStyle = '#ff0000';
    for (let k = 0; k < boxes.length; k++) {
        const i = Math.floor(k / 9), j = k % 9;
        const box = boxes[k];
        if (box.isEmpty) {
            ctx.font = 'bold ' + Math.ceil(box.height * 3 / 4) + 'px simsun';
            ctx.fillText(cells[i][j].textContent, box.x + Math.floor(box.width / 3), box.y + Math.ceil(box.height * 0.75));
        }
    }
    // 将canvas内容以图像URL的形式导出
    const dataURL = canvasElm.toDataURL();
    imageElm.src = dataURL;
};

window.checkAnswer = function () {
    statusElm.innerHTML = isValidSudoku(cells) ? "答案正确" : "答案不正确";
}

document.getElementById('pasteArea').addEventListener('paste', function (e) {
    // 阻止默认的粘贴行为
    e.preventDefault();

    // 检查粘贴的数据是否包含图片
    if (e.clipboardData && e.clipboardData.items) {
        const items = e.clipboardData.items;
        for (let i = 0; i < items.length; i++) {
            if (items[i].type.indexOf('image') !== -1) {
                resetWorkspace();
                const blob = items[i].getAsFile();
                rawImage.src = URL.createObjectURL(blob);
                imageElm.src = rawImage.src;
                statusElm.innerHTML = "处理中...";
                break
            }
        }
    }
});

function sudokuSolver(data) {
    for (let i = 0; i < 9; i++) {
        for (let j = 0; j < 9; j++) {
            if (data[i][j] === '0') { // 如果当前位置为空
                for (let k = 1; k <= 9; k++) { // 尝试填入数字1-9
                    if (isValid(data, i, j, k)) {
                        data[i][j] = k.toString();
                        if (sudokuSolver(data)) {
                            return true; // 如果找到一个有效的解，立即返回
                        }
                        data[i][j] = '0'; // 回溯，撤销当前数字的填入
                    }
                }
                return false; // 尝试所有数字均无效，返回false
            }
        }
    }
    return true; // 所有位置均正确填满，返回true
}

function isValid(board, row, col, k) {
    for (let i = 0; i < 9; i++) {
        const m = 3 * Math.floor(row / 3) + Math.floor(i / 3);
        const n = 3 * Math.floor(col / 3) + i % 3;
        if (board[row][i] == k || board[i][col] == k || board[m][n] == k) {
            return false;
        }
    }
    return true;
}

rawImage.onload = async function () {
    const mat = cv.imread(rawImage);
    URL.revokeObjectURL(rawImage.src);

    // 图片预处理，二值化;
    const thresh = new cv.Mat();
    cv.cvtColor(mat, thresh, cv.COLOR_RGBA2GRAY, 0);
    cv.threshold(thresh, thresh, 180, 255, cv.THRESH_BINARY_INV);

    // 找到网格线   
    const lines = findLines(thresh);
    if (!((lines.h.length === 10) && (lines.v.length === 10))) {
        statusElm.innerHTML = '查找网格线出错';
        return null;
    }

    let result = {};
    await recognizeByRow(thresh, lines).then((ret) => { result = ret; })
        .catch((err) => {
            statusElm.innerHTML = "整行识别失败...";
            console.error('整行识别发生错误:', err);
        });

    if (result.full) {
        fillIn(result.board, cells);
        statusElm.innerHTML = "";
        return null;
    }

    boxes = findBoxes(lines);
    await recognizeByBoxes(thresh, boxes).then((ret) => { result = ret; statusElm.innerHTML = ""; })
        .catch((err) => {
            statusElm.innerHTML = "单字识别失败...";
            console.error('单字识别发生错误:', err);
        });;
    thresh.delete();
    mat.delete();

    sudokuSolver(result.board);
    fillIn(result.board, cells);
    if (!(result.full)) {
        genOutputPic();
    }
};

checkPaddleOCRLoaded();
